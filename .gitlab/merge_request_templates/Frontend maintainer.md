/label ~Frontend ~"trainee maintainer"
/assign `@manager`

<!-- Congratulations! Fill out the following MR when you feel you are ready to become -->
<!-- a frontend maintainer! This MR should contain updates to a file in `data/team_members/person/` -->
<!-- declaring yourself as a maintainer of `gitlab-org/gitlab-ui` and -->
<!-- `gitlab-org/gitlab-com` -->

## Self-assessment

It's hard to specify hard requirements for becoming a maintainer, which is why [the documentation](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-project-maintainer) consists of flexible guidelines. Nevertheless, it's still important to have a more formal list to help both a candidate and their manager to assess the "readiness" of the candidate.

- [ ]  Advanced understanding of the GitLab project as per [the documentation](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-project-maintainer):
  > good feel for the project codebase, expertise in one or more domains, and deep understanding of our coding standards
- [ ] The MRs reviewed by the candidate consistently make it through maintainer review without significant additionally required changes.
- [ ] The MRs authored by the candidate consistently make it through reviewer and maintainer review without significant required changes.

## Links to Non-Trival MRs I've Reviewed

<-- interesting MRs reviewed and discussions in trainee issue here -->

## Links to Non-Trivial MRs I've Written

<-- interesting MRs authored here -->

## Examples of challenging situations

Have there been times you authored code that introduced a bug? Have you ever missed something in a merge request you reviewed? What about a merge request that felt like an endless cycle of reviews? Or maybe you did not agree with a reviewer or authors approach? If so, now is the time to tell us about it! 

Don't worry we all make mistakes and we don't all agree all of the time, there is no negative judgment with this section, simply an invitation to show how you overcome hard moments and adapt.

### Why

We want to understand how you adapt to difficult moments working in the GitLab codebase. 

No negative judgment here; everyone makes mistakes, and if you can't find any examples, you probably haven't ventured outside your comfort zone. Effective maintainers approach challenging and unfamiliar situations with a teachable and ego-less spirit. 

Tell us about:

- Mistakes you've made?
- How you reacted?
- What you learned?

<-- interesting MRs either authored or reviewed that introduced a bug or regression -->

`@gitlab-org/maintainers/frontend` please chime in below with your thoughts, and
approve this MR if you agree. <!-- once you are ready to get feedback from the group, remove the backticks around the mention -->

## Once This MR is Merged

1. [ ] Create an [access request][access-request]
       for maintainer access to `gitlab-org/<project>`. <!-- make sure to update the <project> as needed, for example `gitlab-org/gitlab` -->
1. [ ] Join the [`[at]frontend-maintainers` slack group][frontend-maintainers-slack-group]
1. [ ] Let a maintainer add you to `gitlab-org/maintainers/frontend`
1. [ ] Announce it _everywhere_
1. [ ] Keep reviewing, start merging :sign_of_the_horns: :sunglasses: :sign_of_the_horns:

[access-request]: https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#individual-or-bulk-access-request
[frontend-maintainers-slack-group]: https://gitlab.slack.com/archives/C9Q5V0597
